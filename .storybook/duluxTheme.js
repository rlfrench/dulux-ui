import { create } from "@storybook/theming/create";

export default create({
  base: "light",

  colorPrimary: "hotpink",
  colorSecondary: "#002C65",

  // // UI
  // appBg: "white",
  // appContentBg: "silver",
  // appBorderColor: "grey",
  // appBorderRadius: 4,

  // // Typography
  // fontBase: '"Open Sans", sans-serif',
  // fontCode: "monospace",

  // Text colors
  textColor: "#002C65",
  textInverseColor: "#FFFFFF",

  // // Toolbar default and active colors
  // barTextColor: "silver",
  // barSelectedColor: "black",
  // barBg: "hotpink",

  // Form colors
  inputBg: "white",
  inputBorder: "#002C65",
  inputTextColor: "#002C65",
  inputBorderRadius: 4,

  brandTitle: "Dulux UI",
  brandUrl: "https://www.dulux.com.au/",
  brandImage:
    "https://www.dulux.com.au/content/dam/dulux/logos/dulux-logo-large.png",
});
