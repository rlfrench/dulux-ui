import { addons } from "@storybook/addons";
import duluxTheme from "./duluxTheme";

addons.setConfig({
  theme: duluxTheme,
});
