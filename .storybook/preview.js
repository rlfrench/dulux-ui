import React from "react";
import { addDecorator } from "@storybook/react";
import "../src/css/base.css";

addDecorator(storyFn => (
  <>
    <div style={{ margin: "1rem" }}>{storyFn()}</div>
  </>
));
