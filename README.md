# Dulux UI

_A design system for Dulux._

## Installation

**Installing Dependecies**

Dulux UI requires you to install the following dependencies on your project for it to work correctly:

**Inter Font**

Install the Inter font by adding the following to the `<head>` of your project:

`<link
  href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
  rel="stylesheet"
/>`

**Tailwind CSS**

Install the tailwindcss library by following the instructions here: [Tailwind Installation](https://tailwindcss.com/docs/installation/).

Make sure you add the following `tailwind.config.js` to your projects root directory.

*Note: you will need to configure the purge paths in the config. This is so tailwind can remove all the unused CSS to keep file size smaller.*



```
const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: ["./src/**/*.js", "./src/**/*.css"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        dulux: "#002C65",
        "dulux-dark": "#001E45",
      },
    },
  },
  variants: {},
  plugins: [],
};
```

**Install Dulux-UI**


Dulux UI is includes multiple react components which you can import one by one. All you need to do is install the `dulux-ui` package:

`yarn add dulux-ui`

or

`npm install dulux-ui --save`

**Importing a component in your project**



```
import { Button } from 'dulux-ui';

<Button />
```


## Local Development

**Setup**

1.  Clone the repository.
2.  Cd into the project directory 'dulux-ui'.
3.  Run `yarn` to install project dependencies.

**Commands**

Start storybook to develop components locally:

`yarn dev`

Build the project into the /dist directory

`yarn build`

Release:

`TBC`
