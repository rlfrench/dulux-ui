const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: ["./src/**/*.js", "./src/**/*.css"],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        dulux: "#002C65",
        "dulux-dark": "#001E45",
      },
    },
  },
  variants: {},
  plugins: [],
};
