import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { text, size, onClick, theme } = props;
  function sizeClasses() {
    switch (size) {
      case 'large':
        return 'px-12 py-6 text-normal';
      case 'medium':
        return 'px-8 py-4 text-sm';
      case 'small':
        return 'px-5 py-3 text-xs';
      default:
        return 'px-5 py-3 text-sm';
    }
  }

  function themeClasses() {
    switch (theme) {
      case 'ghost':
        return 'border border-dulux text-dulux hover:bg-dulux focus:bg-dulux hover:text-white';
      case 'solid':
        return 'bg-dulux hover:bg-dulux-dark focus:bg-dulux-dark';
      default:
        return 'bg-dulux';
    }
  }

  return (
    <button
      type='button'
      onClick={() => onClick}
      className={`font-sans inline-flex text-white cursor-pointer uppercase font-semibold tracking-widest ${sizeClasses()} ${themeClasses()}`}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  size: PropTypes.oneOf(['large', 'medium', 'small']),
  theme: PropTypes.oneOf(['green', 'yellow', 'red']),
};

Button.defaultProps = {
  text: 'Button',
  onClick: null,
  size: 'medium',
  theme: 'solid',
};

export default Button;
