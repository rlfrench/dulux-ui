import React from 'react';
import { withA11y } from '@storybook/addon-a11y';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';
import Button from '../src/Button';

export default {
  title: 'Button',
  component: Button,
  decorators: [withA11y, withKnobs],
};

export const Standard = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='medium'
    theme='solid'
  />
);

export const SolidLarge = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='large'
    theme='solid'
  />
);

export const SolidMedium = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='medium'
    theme='solid'
  />
);

export const SolidSmall = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='small'
    theme='solid'
  />
);

export const GhostLarge = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='large'
    theme='ghost'
  />
);

export const GhostMedium = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='medium'
    theme='ghost'
  />
);

export const GhostSmall = () => (
  <Button
    onClick={action('clicked button')}
    text={text('Text', 'Contact Us')}
    size='small'
    theme='ghost'
  />
);
