import React from 'react';
import PropTypes from 'prop-types';

const Heading = (props) => {
  const { size, bold, children } = props;
  function headingElement() {
    switch (size) {
      case 'h1':
        return (
          <h1 className={`text-dulux-dark text-6xl ${bold && 'font-bold'}`}>
            {children}
          </h1>
        );
      case 'h2':
        return (
          <h2 className={`text-dulux-dark text-5xl ${bold && 'font-bold'}`}>
            {children}
          </h2>
        );
      case 'h3':
        return (
          <h3 className={`text-dulux-dark text-4xl ${bold && 'font-bold'}`}>
            {children}
          </h3>
        );
      case 'h4':
        return (
          <h4 className={`text-dulux-dark text-3xl ${bold && 'font-bold'}`}>
            {children}
          </h4>
        );
      case 'h5':
        return (
          <h5 className={`text-dulux-dark text-2xl ${bold && 'font-bold'}`}>
            {children}
          </h5>
        );
      case 'h6':
        return (
          <h6 className={`text-dulux-dark text-2xl ${bold && 'font-bold'}`}>
            {children}
          </h6>
        );
      default:
        return <h1>{children}</h1>;
    }
  }
  return <>{headingElement()}</>;
};

Heading.propTypes = {
  size: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
  bold: PropTypes.bool,
  children: PropTypes.node,
};

Heading.defaultProps = {
  size: 'h1',
  bold: false,
  children: [],
};

export default Heading;
