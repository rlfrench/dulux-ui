import React from 'react';
import { withA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs';
import Heading from '../src/Heading';

export default {
  title: 'Heading',
  component: Heading,
  decorators: [withA11y, withKnobs],
};

export const Standard = () => <Heading>Heading</Heading>;

export const H1 = () => <Heading size='h1'>Heading</Heading>;
export const H2 = () => <Heading size='h2'>Heading</Heading>;
export const H3 = () => <Heading size='h3'>Heading</Heading>;
export const H4 = () => <Heading size='h4'>Heading</Heading>;
export const H5 = () => <Heading size='h5'>Heading</Heading>;
export const H6 = () => <Heading size='h6'>Heading</Heading>;

export const H1Bold = () => (
  <Heading bold size='h1'>
    Heading
  </Heading>
);
export const H2Bold = () => (
  <Heading bold size='h2'>
    Heading
  </Heading>
);
export const H3Bold = () => (
  <Heading bold size='h3'>
    Heading
  </Heading>
);
export const H4Bold = () => (
  <Heading bold size='h4'>
    Heading
  </Heading>
);
export const H5Bold = () => (
  <Heading bold size='h5'>
    Heading
  </Heading>
);
export const H6Bold = () => (
  <Heading bold size='h6'>
    Heading
  </Heading>
);
